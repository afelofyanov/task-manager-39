package ru.tsc.felofyanov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @Select("SELECT id, login, password, email, fst_name, mid_name, last_name, locked, role FROM tm_user " +
            "WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "lastName", column = "last_name")
    })
    User findByLogin(@Param("login") @Nullable String login);

    @Nullable
    @Select("SELECT id, login, password, email, fst_name, mid_name, last_name, locked, role FROM tm_user " +
            "WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "lastName", column = "last_name")
    })
    User findByEmail(@Param("email") @Nullable String email);

    @Select("DELETE FROM tm_user WHERE login = #{login}")
    int removeByLogin(@Param("login") @Nullable String login);

    @Insert("INSERT INTO tm_user (id, login, password, email, fst_name, mid_name, last_name, locked, role) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{middleName}, #{lastName}, " +
            "#{locked}, #{role})")
    void add(@Nullable User model);

    @Insert("INSERT INTO tm_user (id, login, password, email, fst_name, mid_name, last_name, locked, role) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{middleName}, #{lastName}, " +
            "#{locked}, #{role})")
    void addAll(@Nullable Collection<User> collection);

    @Update("UPDATE tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, " +
            "fst_name = #{firstName}, mid_name = #{middleName}, last_name = #{lastName}, locked = #{locked}, " +
            "role = #{role} WHERE id = #{id}")
    int update(@NotNull User user);

    @NotNull
    @Select("SELECT id, login, password, email, fst_name, mid_name, last_name, locked, role FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "lastName", column = "last_name")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT id FROM tm_user WHERE id = #{id} LIMIT 1")
    User findOneById(@Param("userId") @Nullable String id);

    @Select("SELECT id FROM tm_user WHERE id = #{id} LIMIT 1 OFFSET #{index}")
    User findOneByIndex(@Param("index") @Nullable Integer index);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    int remove(@Nullable User model);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeAll(@Nullable Collection<User> collection);

    @Select("SELECT COUNT(1) FROM tm_user")
    long count();
}
