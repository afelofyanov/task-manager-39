package ru.tsc.felofyanov.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();
}
