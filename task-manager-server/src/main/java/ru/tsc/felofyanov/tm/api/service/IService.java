package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.api.repository.IRepository;
import ru.tsc.felofyanov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
