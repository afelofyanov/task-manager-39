package ru.tsc.felofyanov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class ServerAboutResponse extends AbstractResponse {

    private String email;

    private String name;
}
