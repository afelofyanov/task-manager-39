package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveBase64Request extends AbstractUserRequest {

    public DataSaveBase64Request(@Nullable String token) {
        super(token);
    }
}
